package uk.co.wowcher

import akka.actor.ActorSystem
import akka.io.IO
import akka.util.Timeout
import spray.can.Http
import spray.http.HttpRequest
import scala.concurrent.duration._
import akka.pattern.ask
import spray.http.HttpResponse

/**
 * Spray and Akka.
 */
object ClientDemo { // extends App {

  implicit val timeout: Timeout = 5.seconds

  // An Actor System needs to be created
  implicit val system = ActorSystem()

  import system.dispatcher

  // For Spray to hook into Akka, need to hook into Extension Point, then send it a message
  val future = IO(Http) ? HttpRequest(uri="http://spray.io")

  future.mapTo[HttpResponse] onComplete { response =>
    println(s"The response is $response")
    system.shutdown()
  }

}
