package uk.co.wowcher

import akka.actor.Actor.Receive
import akka.actor.{ActorLogging, Actor, Props, ActorSystem}
import akka.io.IO
import spray.can.Http
import spray.http.{HttpResponse, Uri, HttpMethods, HttpRequest}

/**
 * Server Demo.
 */
object ServerDemo extends App {

  implicit val system = ActorSystem()

  val handler = system.actorOf(Props[DemoService],name="handler")
  // handler is actor
  IO(Http) ! Http.Bind(handler, "localhost", 8080)

}


class DemoService extends Actor with ActorLogging {
  def receive: Receive = {
    case _: Http.Connected => {
      log.info("Registering a new connection")
      sender() ! Http.Register(self)
    }
    case HttpRequest(HttpMethods.GET, Uri.Path("/ping"), _, _, _) =>
      sender() ! HttpResponse(entity = "Pong!")
    case HttpRequest(HttpMethods.GET, Uri.Path("/pong"), _, _, _) =>
      sender() ! HttpResponse(entity = "Ping!")
    case ev: Http.ConnectionClosed => {
      log.info("Connection closed")
    }
    case _: HttpRequest => sender() ! HttpResponse(status = 404 ,entity = "Unknown Resource" )
  }
}