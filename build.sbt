name := """spray-akka"""

version := "1.0"

scalaVersion := "2.11.6"

resolvers += "spray repo" at "http://repo.spray.io"
resolvers += "spray nightlies repo" at "http://nightlies.spray.io"

libraryDependencies ++= {
  val sprayVersion      = "1.3.1"
  val akkaVersion       = "2.3.9"
  val scalaTestVersion  = "2.2.2"
  Seq(
    "io.spray"                %% "spray-can"                       % sprayVersion,
    "io.spray"                %% "spray-routing"                   % sprayVersion,
    "io.spray"                %% "spray-json"                      % "1.3.1",
    "org.scala-lang.modules"  %% "scala-xml"                       % "1.0.3",
    "com.typesafe.akka"       %% "akka-actor"                      % akkaVersion,
    "ch.qos.logback"          %  "logback-classic"                 % "1.1.2",
    "com.typesafe.akka"       %% "akka-testkit"                    % akkaVersion        % "test",
    "io.spray"                %% "spray-testkit"                   % sprayVersion       % "test",
    "org.scalatest"           %% "scalatest"                       % scalaTestVersion   % "test")
}

fork in run := true

Revolver.settings
